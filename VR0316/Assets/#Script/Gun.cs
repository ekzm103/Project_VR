﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun : MonoBehaviour {

    [System.Serializable]
    public class GunConfig
    {
        public float damage;
        public float gunRange;
        public float fireRate;
        public Transform firePoint;
    }

    public bool isFire { get; set; }
    public bool isStopFire { get; set; }

    [SerializeField] private GunConfig gunConfig;

    public GunConfig baseConfig
    {
        get
        {
            return gunConfig;
        }
    }

    protected abstract void Try_Attack();

    public void StartFire()
    {
        StartCoroutine("Firing");
    }

    public void StopFire()
    {
        StopCoroutine("Firing");
    }

    protected abstract IEnumerator Firing();


}