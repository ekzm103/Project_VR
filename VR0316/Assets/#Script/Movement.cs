﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField] private float speed;
    public float Speed { get { return speed; } set { speed = value; } }

    [SerializeField] Transform eyePosition;

    private CharacterController charaterController;

    private Vector3 moveVector = Vector3.zero;
    private void Awake()
    {
        charaterController = GetComponent<CharacterController>();

    }

    private void Update()
    {
   //     charaterController.transform.position = eyePosition.position;
        moveVector = Physics.gravity;
        charaterController.Move(moveVector * Time.deltaTime);
    }

    public void Move(float pos)
    {
        transform.position += eyePosition.forward * Time.deltaTime * pos * speed;
    }
}
