﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour {

    [SerializeField] private int hp;


    private void Update()
    {
        if(hp <= 0)
        {
            Debug.Log("Die");
        }
    }
    public void TakeDamage(int damage)
    {      
        hp -= damage;
    }


}
