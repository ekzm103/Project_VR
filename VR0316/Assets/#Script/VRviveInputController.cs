﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public enum GunType
{
    Pistol = 0,
    HeavyMachine = 1,

}

public class VRviveInputController : MonoBehaviour
{

    [SerializeField] SteamVR_TrackedObject sTrackObject = null;
    [SerializeField] SteamVR_Controller.Device sDevice;
    [SerializeField] private Animator animator;
    [SerializeField] private Movement movement;
    [SerializeField] private GameObject[] gun;
    [SerializeField] private int handNumber; // left 0 // right 1

    private int gunNumber = 0;
    private GunType gunType = new GunType();

    private void Awake()
    {
        sTrackObject = GetComponent<SteamVR_TrackedObject>();
        gunType = GunType.Pistol;

    }

    private void Update()
    {
        sDevice = SteamVR_Controller.Input((int)sTrackObject.index);

        if (handNumber == 1)
        {
            GunChange();
            #region Trigger
            //Down
            if (sDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                VibrateController();
                FireKeyDown();
            }
            //Up
            if (sDevice.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                FireKeyUp();
            }
            //Value
            Vector2 triggetValue = sDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Trigger);
            #endregion

            #region Grip
            //Down
            if (sDevice.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                Debug.Log("Grip Down");
            }
            //Up
            if (sDevice.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
            {
                Debug.Log("Grip Up");
            }
            #endregion

            #region TouchPad
            //Down
            if (sDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
            {
                gunNumber++;
                if (gunNumber >= gun.Length)
                    gunNumber = 0;

            }

            //Up
            if (sDevice.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
            {
                Debug.Log("TouchPad Up");
            }

            Vector2 touchValue = sDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

            #endregion
        }
        if (handNumber == 0)
        {
            #region Trigger
            //Down
            if (sDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
            {

            }
            //Up
            if (sDevice.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
            {

            }
            //
            if (sDevice.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
            {
                movement.Move(1);
                Debug.Log("Trigger On");
            }

            //Value
            Vector2 triggetValue = sDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Trigger);
            #endregion

            #region Grip
            //Down
            if (sDevice.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                Debug.Log("Grip Down");
            }
            //Up
            if (sDevice.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
            {
                Debug.Log("Grip Up");
            }
            #endregion

            #region TouchPad
            //Down
            if (sDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
            {


            }

            //Up
            if (sDevice.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
            {
                Debug.Log("TouchPad Up");
            }

            if (sDevice.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
            {
                movement.Move(-1);
                Debug.Log("Touchpad On");
            }

            Vector2 touchValue = sDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

            #endregion
        }
    }

    public void FireKeyDown()
    {
        switch (gunType)
        {
            case GunType.Pistol:
                gun[0].GetComponent<PistolGun>().StartFire();
                animator.SetTrigger("isFire");
                break;

            case GunType.HeavyMachine:
                gun[1].GetComponent<HeavyMachineGun>().StartFire();
                break;
        }
    }
    public void FireKeyUp()
    {
        switch (gunType)
        {
            case GunType.Pistol:
                gun[0].GetComponent<PistolGun>().StopFire();
                break;

            case GunType.HeavyMachine:
                gun[1].GetComponent<HeavyMachineGun>().StopFire();
                break;
        }
    }

    public void GunChange()
    {
        switch (gunNumber)
        {
            case 0:
                gunType = GunType.Pistol;
                gun[0].SetActive(true);
                gun[1].SetActive(false);
                break;

            case 1:
                gunType = GunType.HeavyMachine;
                gun[0].SetActive(false);
                gun[1].SetActive(true);
                break;
        }
    }

    void VibrateController()
    {
        sDevice.TriggerHapticPulse(5000);
    }

}