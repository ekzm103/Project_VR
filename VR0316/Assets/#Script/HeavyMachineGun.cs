﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyMachineGun : Gun {

    [SerializeField] private ParticleSystem[] fireEffect;
    [SerializeField] private GameObject wallEffect;
    [SerializeField] private GameObject hitEffect;

    protected override void Try_Attack()
    {
        TargetEffect(wallEffect, "Wall");
        TargetEffect(hitEffect, "Zombie01");
    }

    public void TargetEffect(GameObject effect , string name)
    {
        RaycastHit hit;

        if (Physics.Raycast(baseConfig.firePoint.position, baseConfig.firePoint.transform.forward, out hit, baseConfig.gunRange))
        {
            if (hit.transform.tag == name)
            {
                GameObject clone = Instantiate(effect);
                clone.transform.LookAt(hit.normal);
                clone.transform.position = hit.point;
            }
        }
    }

    protected override IEnumerator Firing()
    {
        for (int i = 0; i < fireEffect.Length; i++)
        {
            fireEffect[i].Play();
        }

        Try_Attack();
        yield return new WaitForSeconds(baseConfig.fireRate);
        StartCoroutine("Firing");
    }



}
