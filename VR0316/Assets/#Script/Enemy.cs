﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public enum EnemyStateType
{
    Idle,
    Trace,
    Attack,
    Patrol,
    Hit,
}

public class Enemy : MonoBehaviour {

    private NavMeshAgent nav;
    [SerializeField] private Transform[] patrol;
    [SerializeField] private Transform   target;
    private Animator enemyAni;

    [SerializeField] private EnemyStateType enemyStateType;
    private bool isTargetTrace;
    private int i = 0;

    private void Awake()
    {
       nav =  GetComponent<NavMeshAgent>();
       enemyAni = GetComponent<Animator>();
      
       StartCoroutine(enemyStateType.ToString());

    }

    private void Update()
    {
        enemyAni.SetFloat("velocity",nav.speed);

        Debug.Log(enemyAni.GetFloat("velocity"));
        float distance = Vector3.Distance(transform.position, target.position);

        if (distance < 10.0f)
            isTargetTrace = true;
        else
            isTargetTrace = false;



        if (isTargetTrace == true)
            TargetPlayer();

        else
            patrolPatton();
  

    }

    void patrolPatton()
    {
        if (i >= patrol.Length)
            i = 0;


        float distance = Vector3.Distance(transform.position, patrol[i].position);

        if (distance > 1.6f)
        {
            nav.destination = patrol[i].position;
        }
        else
        {
            i++;
        }



    }

    void TargetPlayer()
    {
        float distance = Vector3.Distance(transform.position, target.position);
        nav.destination = target.position;

   
    }

    void enemyStateTypes(EnemyStateType newEnemyType)
    {
        StopCoroutine(enemyStateType.ToString());
        enemyStateType = newEnemyType;
        StartCoroutine(enemyStateType.ToString());
    }
    private IEnumerator Idle()
    {
        while (true)
        {
            float distance = Vector3.Distance(transform.position, target.position);
            nav.speed = 0;

            if (distance < 15.0f)
                enemyStateTypes(EnemyStateType.Trace);

            

            yield return null;
        }
    }

    private IEnumerator Trace()
    {
        while (true)
        {
            nav.speed = 2.0f;
            nav.destination = target.position;
            enemyAni.SetBool("isRun", true);

            float distance = Vector3.Distance(transform.position, target.position);
            if (distance > 15.0f)
                enemyStateTypes(EnemyStateType.Idle);

            if (distance < 5.0f)
                enemyStateTypes(EnemyStateType.Attack);

            yield return null;
        }
    }

    private IEnumerator Attack()
    {
        while (true)
        {
            if (!enemyAni.GetBool("isAttack"))
            {
                float distance = Vector3.Distance(transform.position, target.position);
                nav.speed = 0.0f;

                if (distance > 5.0f && distance < 15.0f)
                    enemyStateTypes(EnemyStateType.Trace);
                else
                    enemyAni.SetBool("isAttack", true);
                
            }


            yield return null;
        }
    }

    private IEnumerator Patrol()
    {
        while (true)
        {
            float distance = Vector3.Distance(transform.position, target.position);
            nav.speed = 0.0f;
            enemyAni.SetBool("isAttack", true);

            if (distance > 5.0f && distance < 15.0f)
                enemyStateTypes(EnemyStateType.Trace);

            yield return null;
        }
    }

    private IEnumerator Hit()
    {

        if(enemyAni.GetBool("isRun")  == true )
        {

        }
        yield return null;
    }

    public void RestartAttack()
    {
        enemyAni.SetBool("isAttack", false);
    }

}
