﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolGun : Gun {

    [SerializeField] private ParticleSystem fireEffect;
    [SerializeField] private GameObject wallEffect;
    [SerializeField] private GameObject hitEffects;

    protected override void Try_Attack()
    {
        TargetEffect(wallEffect, "Wall");
        TargetEffect(hitEffects, "Zombie01");
    }



    public void TargetEffect(GameObject effect , string name)
    {
        RaycastHit hit;

        if (Physics.Raycast(baseConfig.firePoint.position, baseConfig.firePoint.transform.forward, out hit, baseConfig.gunRange))
        {
            if (hit.transform.tag == name)
            {
                GameObject clone = Instantiate(effect);
                clone.transform.LookAt(hit.normal);
                clone.transform.position = hit.point;
            }
        }
    }

    protected override IEnumerator Firing()
    {
        fireEffect.Play();
        Try_Attack();
        yield return new WaitForSeconds(baseConfig.fireRate);

    }



}
